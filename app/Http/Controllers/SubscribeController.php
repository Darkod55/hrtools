<?php

namespace App\Http\Controllers;
use App\Http\Requests\SubscribeValidationRequest;
use Illuminate\Http\Request;
use App\Subscribe;
use Session;

class SubscribeController extends Controller
{
    public function storeSubscribe(SubscribeValidationRequest $request) {

        $subscribe = new Subscribe();
        $subscribe->email = $request->get('email');
        $subscribe->status = 1;
        $subscribe->save();

    
        return response()->json(['message'=>'Thanks for subscribing']);
        // dd(session()->all());
        // return redirect()->route('Index');
    }
}
