<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Contributors;

class HomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $featured = Post::with('categories')->orderBy('views', 'desc')->first();
        $posts = Post::with('categories')->orderBy('views', 'desc')->get();
        // return $posts;
        // $posts = compact('posts');
        // dd(Post::with('categories')->orderBy('views', 'desc')->get());
        return view('index', ["categories" => $categories, 'posts' => $posts, 'featured' => $featured]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showHome() {

        $posts = Post::with('categories')->orderBy('views', 'desc')->get();
        return $posts;

    }
    public function showFeatured() {

        $posts = Post::with('categories')->orderBy('views', 'desc')->first();
        return $posts;

    }

    public function search(Request $request) {
        $posts = Post::where('post_title', 'LIKE', '%'.$request->get('search').'%')->orWhere('content', 'LIKE', '%'.$request->get('search').'%')->with('categories')->get();

        return response()->json($posts);
    }

    public function storePost(Request $request)
    {
        $contributor = new Contributors();
        $contributor->email = $request->get('email');
        $contributor->save();

        $post = new Post();
        $post->post_title = $request->get('post_title');
        $post->link = $request->get('link');
        $post->summary = $request->get('summary');
        $post->content = " ";
        $post->featured_image = "/assets/images/placeholder.jpg";
        $post->views = 0;
        $post->status = 0;
        $post->contributor_id = 1;
        $post->save();

        $post->categories()->attach($request->category_id);

        
        return response()->json(['message'=>'Thank you for your support, we will review the product and include it in the list!']);
        // return redirect()->route('Index');
    }
}