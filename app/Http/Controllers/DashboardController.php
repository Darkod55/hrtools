<?php

namespace App\Http\Controllers;
use App\Company;
use App\Subscribe;
use App\Post;
use App\Category;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function showCompanies() {
        $companies = Company::all();
        return $companies;
    }

    public function showSubscribers() {
        $subscribers = Subscribe::all();
        return $subscribers;
    }

    public function showPosts() {
        $posts = Post::all();
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::destroy($id);
        return redirect()->back();
    }

    public function approve($id)
    {
        Company::where('id', $id)->update(['is_approved' => 1]);
        return redirect()->back();
    }

    public function disapprove($id)
    {
        Company::where('id', $id)->update(['is_approved' => 0]);
        return redirect()->back();
    }

    public function destroySubscriber($id)
    {
        Subscribe::destroy($id);
        return redirect()->back();
    }

    public function destroyPost($id)
    {
        Post::destroy($id);
        return redirect()->back();
    }

    public function editPost($id)
    {
        $post = Post::find($id);
        $categories = Category::all();
        return view('editPost', ['categories' => $categories, "post" => $post]);
    }
    public function updatePost(Request $request) {

        Post::where('id', $request->id)->update([
            'post_title' => $request->get('post_title'),
            'featured_image' => $request->get('featured_image'),
            'summary' => $request->get('summary'),
            'content' => $request->get('content'),
            'link' => $request->get('link'),
        ]);

        return redirect()->route('IndexDash'); 
    }
    public function approvePost($id)
    {
        Post::where('id', $id)->update(['status' => 1]);
        return redirect()->back();
    }
}
