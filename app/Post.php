<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = true;

    public function categories() {
        return $this->belongsToMany(Category::class, 'category_post');
    }

    public function contributors() {
        return $this->belongsTo(Contributors::class, 'contributors_id');
    }
}
