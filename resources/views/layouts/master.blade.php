<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'Hakaton Final')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel='icon' href="{{ asset('assets/images/favicon.png') }}" type='image/x-icon' />
        @include('layouts.style')
        @include('layouts.scripts')
    </head>
    <body>
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </body>
</html>
