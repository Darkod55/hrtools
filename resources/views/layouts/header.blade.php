<header class="navbar-fixed-top">
<nav class="navbar navbar-default navcolony navbar-inverse">
  <div class="container-fluid headersecond">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed navbar1" data-toggle="collapse" data-target="#navbar1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand logocolony" href="https://meetcolony.com/"><img class="logoclass" src="assets/images/colony-01.png" /></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar1">
      <ul class="nav navbar-nav">
        <li><a href="https://meetcolony.com/services">Services</a></li>
        <li><a href="https://meetcolony.com/company">Company</a></li>
        <li><a href="https://meetcolony.com/your-talents">Our Talents</a></li>
        <li><a href="https://meetcolony.com/community">Community</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><button class="btn btncolony"><a href="https://meetcolony.com/build">Start Building Your Team</a></button></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<nav class="navbar navbar-default navcolony">
  <div class="container-fluid headersecond">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header hrtoolsheader">
      <a href="https://meetcolony.com/hrtools"><img class="logo navbar-left" src="{{ asset('assets/images/hrlogo.png') }}" /></a>
      <button type="button" class="navbar-toggle collapsed navbar2" data-toggle="collapse" data-target="#navbar2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <form id="searchForm" class="navbar-form nav-line forsearch" action="app/api/forsearch.php" method="GET">
          <div class="form-group">
              <div class="input-group">
                  <span class="input-group-addon" id="search-icon"><span class="glyphicon glyphicon-search search-button"></span></span>
                  <input type="text" id="search-input" name="searchput" class="form-control for-search  myInput" aria-describedby="search">
              </div>
              <!-- <button type="submit" class="btn btn-default">Submit</button> -->
          </div>
      </form>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar2">
      <ul class="nav navbar-nav onlymobile">
          @php 
            $categories = \App\Category::all()
          @endphp
          @foreach($categories as $category)
              <p class="cat-item text-uppercase cat{{ $category->id }}" data-slug="{{ $category->slug }}">{{ $category->title }}</p>
          @endforeach
        </ul>
        @if(Session::has('message'))
            <div class="alert alert-success" role="alert">
                {{Session::get('message')}}
            </div>
        @endif
      <form class="navbar-form navbar-right nomobile subscribeForm" method="POST" action="{{ route('storeSubscribe') }}">
        @csrf
        <div class="form-group custom-form-group navbar-right">
          <span id="mail-feedback"></span>
              <div class="form-group flex custom-form-group">
                  <div class="input-group">
                      <span class="input-group-addon custom-form-icon"><i class="far fa-envelope"></i></span>
                      <input type="email" id="subscribe" name="email" required class="form-control custom-form" placeholder="Get the Latest Tools" aria-describedby="basic-addon1">
                  </div>
                  <button class="btn submitBtn" type="submit" id="subscribe_mail" name="submit_mail" value="Submit">Find out first!</button>
              </div>
          </div>
      </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header>