<footer class="gray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>Made with <i class="fas fa-heart"></i> from Brainster Academy</p>
            </div>
        </div>
    </div>
</footer>