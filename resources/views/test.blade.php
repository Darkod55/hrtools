<!DOCTYPE html>
<html>
	<head>
		<title>Side Navbar Example</title>
		<meta charset='UTF-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel='stylesheet' type='text/css' href='assets/dist/css/bootstrap-side-nav.css'>
		<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/28330b3406.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
<script src="assets/js/main.js"></script>
	</head>
	<body>
		<!-- Standard bootstrap navbar fixed top -->
		<nav class='navbar navbar-default navbar-fixed-top'>
			<div class='container-fluid'>   
				<div class='navbar-header'>
					<a href='example.html'><img src='assets/images/hrlogo.png' style='width:256px;height:50px;'></a>
					<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
					<span class='sr-only'>Toggle navigation</span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
                    </button>
                    <form id="searchForm" class="navbar-form nav-line forsearch" action="app/api/forsearch.php" method="GET">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon" id="search-icon"><span class="glyphicon glyphicon-search search-button"></span></span>
                        <input type="text" id="search-input" name="searchput" class="form-control for-search  myInput" aria-describedby="search">
                    </div>
                    <!-- <button type="submit" class="btn btn-default">Submit</button> -->
                </div>
            </form>
				</div>
				<div id='navbar' class='navbar-collapse collapse' aria-expanded='false'>
					<ul class='nav navbar-nav side-nav'>
						<li><a href='example.html'><i class='glyphicon glyphicon-home'></i> Home</a></li>
						<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-dashboard'></i> Dashboard</a></li>
						<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-tag'></i> Sales</a></li>
						<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-envelope'></i> Messages</a></li>
						<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-pencil'></i> Edit</a></li>
						<li class='dropdown'>
							<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>
							<i class='glyphicon glyphicon-book'></i> Dropdown Menu <span class='caret'></span>
							</a>
							<ul class='dropdown-menu'>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-play-circle'></i> Sub Menu 1</a></li>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-play-circle'></i> Sub Menu 2</a></li>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-play-circle'></i> Sub Menu 3</a></li>
							</ul>
						</li>
						<li class='side-nav-padding'></li>
						<!-- ADD THIS IN HERE IF YOU'RE NOT USING navbar-right
										|
										|
										v
										
						<li class='right-nav-padding'></li>
						
										^
										|
										|
						ADD THIS IN HERE IF YOU'RE NOT USING navbar-right -->
					</ul>
					<ul class='nav navbar-nav navbar-right'>
						<li class='dropdown'>
							<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>
							<i class='glyphicon glyphicon-user'></i> Username <span class='caret'></span>
							</a>
							<ul class='dropdown-menu'>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-user'></i> Profile</a></li>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-cog'></i> Account settings</a></li>
								<li role='separator' class='divider'></li>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-info-sign'></i> Help</a></li>
								<li role='separator' class='divider'></li>
								<li><a href='#' onclick='activeBtn(this);'><i class='glyphicon glyphicon-log-out'></i> Log out</a></li>
							</ul>
						</li>
						<!-- REQUIRED IF YOU'RE USING navbar-right -->
						<li class='right-nav-padding'></li>
						<!-- REQUIRED IF YOU'RE USING navbar-right -->
					</ul>
				</div>
			</div>
		</nav>
		<div class='wrapper'>
			<!-- Page Content -->
			<h1>Welcome</h1>
			<!-- Page Content -->
		</div>
	</body>
	<script type='text/javascript'>
		function activeBtn(elm) {
			$("li").removeClass("active");
			$(elm).parent().addClass("active");
		}
	</script>
</html>