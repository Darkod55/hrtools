@extends('layouts.master')

@section('title')
Homepage
@endsection

@section('content')
<div class="container-fluid homepage">
    <div class="row">
        <div class="col-md-3">
            <div class="sidenav fordesktop">
                @foreach($categories as $category)
                    <p class="cat-item text-uppercase cat{{ $category->id }}" data-slug="{{ $category->slug }}">{{ $category->title }}</p>
                @endforeach
                <p class="cat-item addthiscomp text-uppercase" id="addcompany">Add New Company</p>
                <div class="forLinkedin cat-item">
                    <a href="https://www.linkedin.com/company/30119880" target="_blank"><i class="fab fa-linkedin-in fa-2x linkedIn"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-9 forpadding">
            <div class="row allposts">
                <div class="col-md-12">
                    <!-- <div class="row featured">
                        <div class="col-md-12 featuredcard">
                            
                        </div>
                    </div> -->
                    <div class="row addtool">
                        <div class="col-md-12 col-tool">
                            <button class="btn buttonAdd mr-5">Add Tool</button>
                            <i class="fal fa-share-alt" id="forshare"></i>
                            <p id="testfield">
                                <a href="http://www.facebook.com/sharer.php?u=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-facebook"></i></a>
                                <a href="https://twitter.com/intent/tweet"><i class="fab fa-twitter-square"></i></a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="row posts">
                        <div class="col-md-12 postscol">

                        </div>
                    </div>
                </div>
            </div> 
            <div class="row addformrow">
                <div class="col-md-9">
                    <div id="login">
                        <div class="errorsmsg alert alert-danger" role="alert"></div>
                        <div class="successmsg alert alert-success" role="alert"></div>
                    <form class="addcompform" action="{{ route('addCompany') }}" method="POST">
                        @csrf
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <button type="button" class="formclose close close-per" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="form-group">
                            <label>Add your company here</label>
                            <input type="text" name="name" class="form-control margin-bottom" id="name" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                            <input type="text" name="website" class="form-control margin-bottom" id="website" placeholder="Company Website">
                        </div>
                        <div class="form-group">
                            <textarea rows="4" cols="50" name="about" class="form-control margin-bottom" id="about" placeholder="About company"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="employee" class="control-label">Do you work for this company?</label><br />
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary radiobtn">
                                    <input class="radio-inline" type="radio" name="employee" value="1" checked>Yes
                                </label>
                                <label class="btn btn-primary radiobtn">
                                    <input class="radio-inline" type="radio" name="employee" value="0">No
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Your email adress</label><span id='emailAddCompany-feedback'></span>
                            <input type="email" name="email" class="form-control margin-bottom" id="emailAddCompany" placeholder="example@example.com">
                        </div>     
                        <div class="form-group">
                            <button type="submit" class="btn btn-default butaddcomp" id="fortest">Submit</button>
                        </div>    
                    </form>
                    </div>
                </div>
            </div>
            <div class="row addformrow addtoolrow">
                <div class="col-md-9">
                    <div id="addnewtool">
                        <div class="errorsmsg alert alert-danger" role="alert"></div>
                        <div class="successmsg alert alert-success" role="alert"></div>
                        <form class="addtoolform" action="{{ route('storePost') }}" method="POST">
                            @csrf
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <button type="button" class="formclose close close-per" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <div class="form-group">
                                <label for="post_title">Product Name <span class="text-danger">*</span></label>
                                <input type="text" name="post_title" class="form-control margin-bottom" id="post_title" placeholder="Please keep it short, e.g. Google Hangouts">
                            </div>
                            <div class="form-group">
                                <label for="link">Website <span class="text-danger">*</span></label>
                                <input type="text" name="link" class="form-control margin-bottom" id="link" placeholder="Website">
                            </div>
                            <div class="form-group">
                                <label for="link">Social Media</label>
                                <input type="text" name="link" class="form-control margin-bottom" id="link" placeholder="Social Media">
                            </div>
                            <div class="form-group">
                                <label for="summary">Product Details <span class="text-danger">*</span></label>
                                <input type="text" name="summary" class="form-control margin-bottom" id="summary" placeholder="Please tell us what does this product do? Keep it short">
                            </div>  
                            <div class="form-group">
                                <label for="category_id" class="control-label">Choose Category</label><br />
                                @php 
                                    $categories = \App\Category::all();
                                @endphp
                                <select name="category_id" class="form-control">
                                    <option selected disabled>Select Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Your email adress <span class="text-danger">*</span></label>
                                <input type="email" name="email" class="form-control margin-bottom" id="emailContributers" placeholder="example@example.com">
                            </div>     
                            <div class="form-group">
                                <button type="submit" class="btn btn-default butaddtool butaddcomp" id="butaddtool">Submit</button>
                            </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@endsection