<!DOCTYPE html>
<html>
    <head>
		<title>Edit Post</title>
		<meta charset='UTF-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'>
		<link rel='icon' href='{{ asset("assets/images/favicon.png") }}' type='image/x-icon' />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel='stylesheet' type='text/css' href='{{ asset("assets/dist/css/bootstrap-side-nav.css") }}'>
		<link rel='stylesheet' type='text/css' href='{{ asset("assets/css/hakaton.css") }}'>
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://kit.fontawesome.com/28330b3406.js" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
		<script src="{{ asset('assets/js/main.js') }}"></script>
	</head>
	<body>
		<!-- Standard bootstrap navbar fixed top -->
		<nav class='navbar navbar-default navbar-fixed-top'>
			<div class='container-fluid'>   
				<div class='navbar-header'>
					<a href="{{ route('Index') }}"><img class="logo" src='/hrtools/assets/images/hrlogo.png' /></a>
					<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar' aria-expanded='false' aria-controls='navbar'>
                        <span class='sr-only'>Toggle navigation</span>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                        <span class='icon-bar'></span>
                    </button>
				</div>
				<div id='navbar' class='navbar-collapse collapse' aria-expanded='false'>
					<ul class='nav navbar-nav navbar-right'>
						<li class="usernamelogin">@if(\Auth::check()) Welcome {{\Auth::user()->name}}! @endif</li>
						<li class='right-nav-padding'></li>
					</ul>
				</div>
			</div>
        </nav>
        <div class="containter">
            <div class="row rowforedit">
                <div class="col-md-8">
                <h2 class="text-center">Edit {{ $post->post_title }}</h2>
                <form method="POST" action="{{ route('UpdatePost') }}">
                    @csrf
                    <input type="hidden" name="id" id="id" value="{{ $post->id }}" /> 
                        <div class="form-group">
                            <label for="post_title">Title</label>
                            <input type="text" name="post_title" id="post_title" class="form-control" value="{{ $post->post_title }}" />
                        </div>
                        <div class="form-group">
                            <label for="featured_image">Image</label>
                            <input type="text" name="featured_image" id="featured_image" class="form-control" value="{{ $post->featured_image }}" />
                        </div>
                        <div class="form-group">
                            <label for="summary">Summary</label>
                            <input type="text" name="summary" id="summary" class="form-control" value="{{ $post->summary }}" />
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea rows="10" type="text" name="content" id="content" class="form-control">{!! $post->content !!}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="link">Link</label>
                            <input type="text" name="link" id="link" class="form-control" value="{{ $post->link }}" />
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success form-control editVehicle">Submit<button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
	</body>
</html>