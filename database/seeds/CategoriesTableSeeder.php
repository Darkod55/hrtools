<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')
            ->insert([
                ['title'  => "Feedback", 'color' => "#EFE4E3", 'slug' => "feedback"],
                ['title'  => "Onboarding", 'color' => "#D7A89F", 'slug' => "onboarding"],
                ['title'  => "Recruiting", 'color' => "#BA5147", 'slug' => "recruiting"],
                ['title'  => "People Analytics", 'color' => "#F06B5E", 'slug' => "peopleanalytics"],
                ['title'  => "Surveys", 'color' => "#CDECE5", 'slug' => "surveys"],
                ['title'  => "Reviews", 'color' => "#8FC0B6", 'slug' => "reviews"],
                ['title'  => "Goals", 'color' => "#8FC0B6", 'slug' => "goals"],
                ['title'  => "Communication", 'color' => "#78A199", 'slug' => "communication"],
                ['title'  => "Recognition", 'color' => "#D7A89F", 'slug' => "recognition"],
                ['title'  => "Payroll and Benefits", 'color' => "#BA5147", 'slug' => "payrollandbenefits"],
                ['title'  => "Employee Records", 'color' => "#78A199", 'slug' => "employeerecords"],
                ['title'  => "Office Experience", 'color' => "#CDECE5", 'slug' => "officeexperience"],
                ['title'  => "Learning", 'color' => "#2D3D39", 'slug' => "learning"],
                ['title'  => "Scheduling", 'color' => "#EFE4E3", 'slug' => "scheduling"],
                ['title'  => "Hiring Remote", 'color' => "#2D3D39", 'slug' => "hiringremote"]
                
            ]);
    }
}
