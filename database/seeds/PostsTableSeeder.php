<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')
            ->insert([
                ['post_title'  => "Teambit", 'featured_image' => "https://teambit.io/wp-content/uploads/facebook-image.png", 'summary' => "Teambit helps teams create high performing cultures by making feedback a daily habit. It's lightweight and seamlessly integrated feedback, surveys, reviews and praise that your team will love.", 'content' => "<h4>Why Teambit exists</h4>
                // Teambit’s mission is to make worklife exciting.
                <br /><br />
                // We are experiencing what Gallup calls an employee engagement crisis. Just one-third of people at work are engaged.
                <br /><br />
                // To improve performance and engagement companies rely on the same principles as industrial-age factories. But all those annual performance reviews and hundred-question surveys don't work anymore. People want regular feedback. Leaders who don't embrace this change end up with misaligned, disengaged and dwindling team.", 'link' => "https://teambit.io/", 'views' => "23", 'status' => '1'],
                ['post_title'  => "Reflektive", 'featured_image' => "https://mms.businesswire.com/media/20191211005024/en/729564/23/Reflektive_wordmark_color_rgb.jpg", 'summary' => "Real-time employee engagement and performance platform.", 'content' => "<h4>Why Reflektive exists</h4>
                Reflektive's mission is to modernize your performance management.
                <br /><br />
                Reflektive is a People Management Suite that helps managers and employees work better together. Reflektive for Chrome is a powerful extension for Gmail that enables managers and employees to give real-time feedback anytime, anywhere.
                <br /><br />
                Connect all your people data into a simple dashboard for always-on insights.Save time and set employees up for success with an intuitive review interface.", 'link' => "https://www.reflektive.com/", 'views' => "3", 'status' => '1'],
                ['post_title'  => "Impraise", 'featured_image' => "https://www-impraise-com-resources.s3.amazonaws.com/uploads/logos/_1200x630_crop_center-center_82_none/impraise-logo-screen.jpg?mtime=1536580403", 'summary' => "Impraise is a web and mobile app for 360 degree feedback and peer coaching", 'content' => "<h4>Why Impraise exists</h4>
                Impraise's mission is to empower people to unleash their potential.
                <br /><br />
                Impraise wants to unleash the potential of individuals and teams, so that they can go forward and further — faster. At Impraise, we believe that seizing moments to celebrate, learn, and improve (no matter the day), can transform roles into careers.", 'link' => "https://www.impraise.com/", 'views' => "12", 'status' => '1'],
                ['post_title'  => "Lattice", 'featured_image' => "https://findlogovector.com/wp-content/uploads/2018/12/lattice-logo-vector.png", 'summary' => "Performance management for growing companies.", 'content' => "<h4>Why Lattice exists</h4>
                Lattice makes performance management software for forward-thinking organizations. 
                <br /><br />
                Lattice helps companies streamline their performance management processes and giving them great software, we hope to give people the best parts of performance management without all the pain. At our best, we help people get the feedback they need to grow, know where they stand, and have a clear idea of how their work moves their organization forward.
                <br /><br />
                People spend most of their waking hours at work - our goal is to make those hours better.", 'link' => "https://lattice.com/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "Small Improvements", 'featured_image' => "https://mk0smallimprovetyqer.kinstacdn.com/wp-content/uploads/2019/11/Artboard.png", 'summary' => "Small Improvements is a performance feedback platform for companies that believe ongoing feedback is essential to employee development.", 'content' => "<h4>Why Small Improvements exists</h4>
                Small Improvements fuels your company’s ongoing feedback culture, and integrates in real-time with collaboration tools such as Slack and Gmail.
                <br /><br />
                Small Improvements is highly configurable. As an admin you can change and combine building blocks to suit your organization’s needs, and adjust the look and feel too. We provide basic structure to save you time and make things easy. In addition you have a wide variety of options to choose from when defining your review forms, 360 processes, and objectives guidelines.", 'link' => "https://www.small-improvements.com/", 'views' => "2", 'status' => '1'],
                ['post_title'  => "Steer", 'featured_image' => "https://miro.medium.com/max/1200/1*1p2rjVSj27mM9xk78ghWPA.png", 'summary' => "OKR, weekly check-ins and daily stand-ups to help you build and maintain engaged, aligned and productive teams no matter where they are.", 'content' => "<h4>Why Steer exits</h4>
                Steer mission is to help managers be their best.
                <br /><br />
                Increase employee performance, reduce employee turnover and help managers be truly leaders in your organization.
                <br /><br />
                Steer helps you drive your team to the right direction by assign them goals, and create a culture of accountability on your team. Everything in one single place.Steer automatically gives you all the feedback in one single place, ready for you to be reviewed and analyzed.", 'link' => "https://www.newsteer.com/", 'views' => "54", 'status' => '1'],
                ['post_title'  => "7Geese", 'featured_image' => "https://7geese.com/wp-content/uploads/2019/01/7Geese_performance_management__meta-image-copy-43.png", 'summary' => "A cloud-based private social network for employees to achieve goals, receive recognition, and gather continuous feedback.", 'content' => "<h4>Why 7Geese exists</h4>
                7Geese mission is to help your teams move forward faster, better, and most importantly, together.
                <br /><br />
                7Geese Objectives provides real-time updates and tracking on OKRs or SMART goals. This enables managers and their teams to overcome roadblocks and celebrate milestones in a timely manner. Gone are the days of spending hours combing through lengthy email chains with out-of-date information.", 'link' => "https://7geese.com/", 'views' => "6", 'status' => '1'],
                ['post_title'  => "Rippling", 'featured_image' => "http://strohlsf.com/wp-content/uploads/rippling/strohl_rippling_logo@2x.png", 'summary' => "Rippling is one directory for employee information across IT, HR, legal, finance and facilities.", 'content' => "<h4>Why Rippling exists</h4>
                Rippling makes it unbelievably easy to manage your team's payroll, benefits, computers, and apps — all in one, modern platform.
                <br /><br />
                Rippling is the underlying system for your employee data. With it, you can automate the 100 little things you need to do when someone joins, works at, or leaves your company.
                Because everything is connected to one, always-accurate employee system of record, you don't have to constantly update rules and employee information. Rippling just works, right out-of-the box, based on your team's titles, departments, roles, and locations.", 'link' => "https://www.rippling.com/", 'views' => "33", 'status' => '1'],
                ['post_title'  => "Zenefits", 'featured_image' => "https://www.zenefits.com/wp-content/uploads/2019/06/thumbnail-zenefits-og.png", 'summary' => "Zenefits online HCM software gives your team a single place to manage all of your HR needs - payroll, benefits, compliance, and more.", 'content' => "<h4>Why Zenefits exists</h4>
                Zenefits has all the HR tools that make managing your people easy, from streamlined onboarding and easy PTO tracking to org charts, performance reviews, and so much more.
                <br /><br />
                Zenefits streamlines your workflow by automatically connecting HR, Benefits, Payroll and Scheduling, together in one platform. This means less time spent on low-priority tasks so your team can focus on more important things.", 'link' => "https://www.zenefits.com/", 'views' => "32", 'status' => '1'],
                ['post_title'  => "Justworks", 'featured_image' => "https://images.ctfassets.net/mnc2gcng0j8q/75kidbqfOrrZXuzpnxz3dz/2ac73c04f775bae19378a0fd0d32d4c0/JW_MetaImages__LOGO.png?w=600", 'summary' => "Justworks is seamless payroll, tax filings, HR support, and access to affordable benefits. It’s an all-in-one solution designed for you to work fearlessly.", 'content' => "<h4>Why Justworks exists</h4>
                Justworks mission is to help entrepreneurs and businesses grow with confidence.
                <br /><br />
                Justworks handles the nitty-gritty of payroll, benefits, compliance, and HR, so you have more time to learn, grow, and seek something worthwhile. Work fearlessly, pursue your passion, and do more of what matters. We’ve got your back.", 'link' => "https://justworks.com/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "Kin", 'featured_image' => "https://kinhr.com/wp-content/uploads/2018/03/KinHR.png", 'summary' => "Kin HR Software streamlines HR for your entire team with employee file and data management, time-off tracking and new hire onboarding.", 'content' => "<h4>Why Kin exists</h4>
                Kin HR software helps companies practice core principles of employee engagement.
                <br /><br />
                Kin centralizes employee data and files, and gets everyone in your organization in the know.
                <br /><br />
                As simple as it sounds, a well-organized digital workplace increases productivity by enabling everyone to contribute. Kin HR software (HRIS) centralizes employee paperwork and data, and keeps everyone in tune as coworkers.", 'link' => "https://kinhr.com/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "Gusto", 'featured_image' => "https://upload.wikimedia.org/wikipedia/commons/a/a0/Photo-pr-logo.jpg", 'summary' => "Gusto offers fully integrated online HR services: payroll, benefits, and everything else.", 'content' => "<h4>Why Gusto exits</h4>
                Gusto makes it easy to onboard, pay, insure, and support your hardworking team.
                <br /><br />
                Through one refreshingly easy, integrated platform, Gusto automate and simplify your payroll, benefits, and HR, all while providing expert support. You and your employees will get the peace of mind you need to do your best work.", 'link' => "https://gusto.com/", 'views' => "13", 'status' => '1'],
                ['post_title'  => "Workday", 'featured_image' => "https://boomi.com/wp-content/uploads/WorkdayLogo.jpg", 'summary' => "Workday offers a highly effective ERP alternative for HR and financial management to businesses.", 'content' => "<h4>Why Workday exists</h4>
                By bringing planning, execution, and analysis together, Workday is designed to help you nimbly shift gears when you need to.
                <br /><br />
                Workday combines finance, HR, and planning in one seamless cloud ERP system for better business performance.
                Workday is ready for anything. Whether it’s complying with new regulations, shifting your workforce, or opening a new location, Workday provides the agility necessary to respond to change.", 'link' => "https://www.workday.com/", 'views' => "15", 'status' => '1'],
                ['post_title'  => "Cake", 'featured_image' => "https://i.ytimg.com/vi/xD8qwJYRHWI/maxresdefault.jpg", 'summary' => "CakeHR is HR management software for small and medium businesses", 'content' => "<h4>Why Cake exists</h4>
                CakeHR mission is to solve your HR challenges and let you focus on building business.
                <br /><br />
                CakeHR has been developing tirelessly with awesome team of developers and designers , improving platform week by week, making this a perfect solution for companies of any size.", 'link' => "https://cake.hr/", 'views' => "4", 'status' => '1'],
                ['post_title'  => "SilkRoad", 'featured_image' => "https://mms.businesswire.com/media/20190124005095/en/702133/23/Full_Logo_Color.jpg", 'summary' => "HR talent management, and activation software that defines the way talent works.", 'content' => "<h4>Why SilkRoad exists</h4>
                SilkRoad Technology enables people to thrive in a changing workplace.
                <br /><br />
                SilkRoad Technology’s software and services help our clients attract, retain and align people to their business.
                <br /><br />
                SilkRoad Technology consulting and advisory services drive strategic and efficient, digital solutions to deepen the connection between people strategy and organizational objectives, ensuring HR leaders are strategic by anticipating and mitigating change management risks.", 'link' => "https://www.silkroad.com/", 'views' => "11", 'status' => '1'],
                ['post_title'  => "Tydy", 'featured_image' => "https://www.wealthmann.com/wp-content/uploads/2019/06/Untitled-1-144-1079x575.png", 'summary' => "Tydy is the solution for employee onboarding & on-going communication.", 'content' => "<h4>Why Tydy exists</h4>
                Tydy's mission is to help organizations of all sizes understand what matters most to their transform the employee experience as a result of it.
                <br /><br />
                Tydy's goal is to make it 'no-code' easy for any one to manage the employee experience at any scale. 
                <br /><br />
                Tydy helps HR know what matters to their employees and build programs around it,  helps each employee know what matters to their jobs and be more productive efficiently, helps managers know what matters to their teams and enable them more, helps executives know what matters to the organization in order to enable success.", 'link' => "https://www.tydy.co/", 'views' => "11", 'status' => '1'],
                ['post_title'  => "Hired", 'featured_image' => "https://hired.com/assets/social-logo-large-b0086412e6700119ac5819af1ac5e6eaf7255a18a6c1e8a299f9c6edb6445507.png", 'summary' => "Hired is a two-sided marketplace that matches technology talent with the right job opportunities.", 'content' => "<h4>Why Hired exists</h4>
                Hired exists to match you with the job you love.
                <br /><br />
                Hired's podcast is a fast paced, rough-and-tumble tour through the strategies, metrics, techniques, and trends shaping the recruitment industry today.", 'link' => "https://hired.com/", 'views' => "2", 'status' => '1'],
                ['post_title'  => "Homerun", 'featured_image' => "https://assets.website-files.com/58d684124f3644b01d649b26/5979c63f40a74f00010278e1_Homerun-General.png", 'summary' => "Rooted in creative culture, Homerun is the one tool for companies to hire great people.", 'content' => "<h4>Why Homerun exists</h4>
                Homerun is designed so everyone likes to work together in one hiring tool.
                <br /><br />
                Homerun customers share a passion for culture, design and innovation. We all agree recruitment should be more meaningful and personal.", 'link' => "https://www.homerun.co/", 'views' => "13", 'status' => '1'],
                ['post_title'  => "Lever", 'featured_image' => "https://www.jobscan.co/blog/wp-content/uploads/lever-ats.png", 'summary' => "Lever streamlines the hiring process and simplifies the Applicant Tracking System, so that recruiters and hiring managers can focus on what truly matters: their candidates.", 'content' => "<h4>Why Lever exists</h4>
                Lever has everything you need to attract, source, and hire top talent at every stage of growth.
                <br /><br />
                Recruiting software to tackle the most strategic problem companies face.
                <br /><br />
                Lever was founded to tackle the most strategic challenge that companies face: how to grow their teams. They're injecting the values we respect – collaboration, transparency, and humanity – into our software and re-imagining how organizations can think about growth, with talent and teamwork at the center.", 'link' => "https://www.lever.co/", 'views' => "41", 'status' => '1'],
                ['post_title'  => "Workable", 'featured_image' => "https://assets.pcmag.com/media/images/570421-workable-logo.jpg", 'summary' => "Hiring software and candidate sourcing for ambitious companies.", 'content' => "<h4>Why Workable exists</h4>
                From AI-powered search and advertising to one-click job posting to 200+ job sites, Workable helps you meet enough great people to choose the best person for the job.
                <br /><br />
                Whether you’re a team of one or one hundred, Workable connects the right people with the right tools and information — so everyone always has what they need to make the right decision.", 'link' => "https://www.workable.com/", 'views' => "13", 'status' => '1'],
                ['post_title'  => "Greenhouse", 'featured_image' => "https://prod-marketing-greenhouse.global.ssl.fastly.net/customer-logos/_1200x630_crop_center-center_82_none/Generic.png?mtime=20190128102927", 'summary' => "Greenhouse is an applicant tracking system and recruiting software designed to optimize your entire recruiting process. Find better candidates, conduct more focused interviews, and make data-driven hiring decisions.", 'content' => "<h4>Why Greenhouse exists</h4>
                Greenhouse makes companies great at hiring, improving the process for everyone involved — from hiring managers and recruiters to candidates themselves.
                <br /><br />
                People are the fundamental source of value for business today. The smartest and fastest-growing companies know Talent is their competitive advantage. Greenhouse provides the technology, resources and expertise to make every company great at hiring.", 'link' => "http://www.greenhouse.io/", 'views' => "23", 'status' => '1'],
                ['post_title'  => "Recruitee", 'featured_image' => "https://i.ytimg.com/vi/dlPCyvn_NXI/maxresdefault.jpg", 'summary' => "Recruitee is a fast, easy, collaborative hiring platform.", 'content' => "<h4>Talent Acquisition Platform.</h4>
                Powerful software that makes hiring easy.
                <br /><br />
                The key to successful hiring is attracting the very best talent.
                <br /><br />
                From the careers site editor to the sourcing extension, our powerful Attract Features help you get the best talent in your pipeline.", 'link' => "https://recruitee.com/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "Tyba", 'featured_image' => "https://scontent-prg1-1.xx.fbcdn.net/v/t1.0-9/56691227_414851065738134_7192389050341261312_o.png?_nc_cat=105&_nc_ohc=zhk9RtTMsJoAX8QcLAZ&_nc_ht=scontent-prg1-1.xx&oh=12d6b61514bcd6b607d00c589b9b2bd8&oe=5E9F8512", 'summary' => "Connecting talent to some of the world's hottest, fast growing startups, and giving people the opportunity to be part of a great team.", 'content' => "Fuel your career on Tyba, the preferred career network for startups and tech companies. Jobs and internships, it’s all there.
                <br /><br />
                Tyba is an international portal focusing on career possibilities across startups and growth companies. ", 'link' => "https://tyba.com/", 'views' => "32", 'status' => '1'],
                ['post_title'  => "JazzHR", 'featured_image' => "https://i.vimeocdn.com/video/781029273_640.jpg", 'summary' => "JazzHR makes recruiting and hiring easy, effective, and scalable no matter what growth looks like to your company.", 'content' => "Find and hire the right talent, fast with the only hiring platform built for businesses like yours.
                <br /><br />
                JazzHR easily integrates with all of your favorite HR tools.
                From importing your new hire information to full benefits management, JazzHR you covered.", 'link' => "https://www.jazzhr.com/", 'views' => "23", 'status' => '1'],
                ['post_title'  => "People.ai", 'featured_image' => "https://www.i2tutorials.com/wp-content/uploads/2019/11/People-ai-i2tutorials.png", 'summary' => "Automate rep training, coaching and Salesforce sync.", 'content' => "People.ai is on a mission to be the AI platform for all business activity across every organization.
                <br /><br />
                People.ai helps sales, marketing, and customer success teams uncover every revenue opportunity from every customer.People.ai is backed by Y Combinator and Silicon Valley’s top investors", 'link' => "https://people.ai/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "Culture Amp", 'featured_image' => "https://i0.wp.com/hrtech.community/wp-content/uploads/2017/05/cultureAmp-logo.png?fit=1030%2C592&ssl=1", 'summary' => "Culture Amp makes it easy to collect, understand and act on employee feedback.", 'content' => "Engagement. Performance. Retention. When it comes to your people and culture, it’s all connected.
                <br /><br />
                Uncover crucial relationships between performance and engagement. Culture Amp turns these insights into high-impact actions to create an ideal employee experience.", 'link' => "https://www.cultureamp.com/", 'views' => "12", 'status' => '1'],
                ['post_title'  => "Peakon", 'featured_image' => "https://peakon.com/wp-content/uploads/2018/09/peakon.jpg", 'summary' => "Peakon is the simple, scientific, and strategic way to maximise your employee engagement, retention, and culture.", 'content' => "Peakon is the world's leading platform for measuring and improving Employee Engagement.
                <br /><br />
                Peakon is a data company - not just a survey company - and this allows to provide insights that will transform your business. Some of Peakon's customers have reduced their churn by 50%, others have completely restructured their organisations. Peakon don't just measure engagement, they drive real change.", 'link' => "https://peakon.com/", 'views' => "7", 'status' => '1'],
                ['post_title'  => "Populate", 'featured_image' => "https://ph-files.imgix.net/d2cfab32-93dc-4302-b9c5-0667324475d0?auto=format&fit=crop&h=512&w=1024", 'summary' => "Create killer headcount & hiring plans, through collaborative sharing & people analytics.", 'content' => "Populate centralizes all of your people data for analysis, collaborative planning and new hire approval. Spend more time adding value to the business and less time updating values in spreadsheets.
                <br /><br />
                With Populate, managers and stakeholders can review and approve headcount or budget changes and make comments, all within the app - say see ya later to endless meetings and emails.", 'link' => "https://populate.io/home", 'views' => "9", 'status' => '1'],
                ['post_title'  => "Polly", 'featured_image' => "https://i.ytimg.com/vi/O1eGsofm-Rk/maxresdefault.jpg", 'summary' => "Engage your team and boost participation with simple and recurring polls in Slack", 'content' => "Polly is on a mission to make it easy for teams to measure their work.
                <br /><br />
                Work smarter, not harder. We'll collect the feedback on your day-to-day processes for you so you can focus on the important stuff. No coding required.
                Supercharge every member of your team to author simple, yet powerful polls that seamlessly integrate into their Slack workflows. Author polls in seconds, and respond with a single click.", 'link' => "https://www.polly.ai/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "CompanyMood", 'featured_image' => "https://www.kraemer-it.de/wp-content/uploads/CompanyMoodMailChimp.png", 'summary' => "CompanyMood is a tool to monitor employee mood and gather feedback.", 'content' => "Due to increasing demand and the belief that regular employee feedback is a key factor in a company's success, since 2014 CompanyMood is helping organizations around the world to measure and sustain employee satisfaction.", 'link' => "https://www.company-mood.com/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "Know Your Company", 'featured_image' => "https://www.metiscomm.com/hubfs/KnowYourTeam-casestudy.png", 'summary' => "Know Your Company is a software tool that helps business owners and employers get to know their employees better.", 'content' => "Know Your Team is software that helps you become a better manager.
                <br /><br />
                Know Your Team merges theory with practice. We give you a Knowledge Center, Tools, and a Community to develop the 3 skills the best managers have: Trust, Honesty, and Context.", 'link' => "https://knowyourcompany.com/", 'views' => "27", 'status' => '1'],
                ['post_title'  => "TinyPulse", 'featured_image' => "https://united-partners.com/wp-content/uploads/2018/04/tiny1.jpg", 'summary' => "TinyPulse helps companies improve retention and happiness by keeping a pulse on how happy, burnt out, and frustrated their team is before retention sinks and issues fester.", 'content' => "TINYpulse is a feedback-based tool that encourages the whole team to provide input. We give leaders quantitative, actionable feedback on problems that are hard to measure and diagnose. This helps create a superior workplace culture and a happier team.", 'link' => "https://www.tinypulse.com/", 'views' => "6", 'status' => '1'],
                ['post_title'  => "Officevibe", 'featured_image' => "https://assets.officevibe.com/wp-content/uploads/2016/04/og_image_officevibe.png", 'summary' => "Officevibe is a survey platform to measure employee engagement.", 'content' => "Future-proof your workplace by empowering managers and engaging teams. Officevibe is a simple platform that helps managers to enhance performance, leaders to understand their people, and organizations to thrive.", 'link' => "https://www.officevibe.com/", 'views' => "38", 'status' => '1'],
                ['post_title'  => "CulturelQ", 'featured_image' => "https://talentoptimization.org/wp-content/uploads/2019/04/CultureIQ.jpg", 'summary' => "CultureIQ, a culture management software company, provides a SaaS soluiton to help companies measure, understand & strengthen their culture.", 'content' => "<h4>Why CultureIQ exists</h4> 
                Consolidate feedback. Throughout the employee lifecycle. With real- time reporting. So you can act.
                <br /><br />
                CultureIQ's flexible platform is easy to use and consolidates all types of feedback from annual, pulse, and employee lifecycle surveys (e.g., onboarding and exit surveys)
                — across the enterprise.", 'link' => "https://cultureiq.com/", 'views' => "16", 'status' => '1'],
                ['post_title'  => "Namely", 'featured_image' => "https://prnewswire2-a.akamaihd.net/p/1893751/sp/189375100/thumbnail/entry_id/0_cpbcov0e/def_height/524/def_width/1000/version/100012/type/2/q/100", 'summary' => "The All-In-One HR Platform.", 'content' => "<h4>Namely is building HR for Humans.</h4> 
                They are providing HR professionals with the technology, data, and support they need to help employees thrive.
                <br /><br />
                Namely's vision is to empower everyone to make data-driven decisions about people. From HR to the C-Suite to employees, they believe that accessible data has the power to make us all more strategic and impactful.", 'link' => "https://www.namely.com/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "PeopleGoal", 'featured_image' => "https://cdn.dribbble.com/users/886289/screenshots/6905148/pg6.jpg", 'summary' => "Cascade goals to your team, develop social feedback and conduct employee 1:1s with real-time performance data.", 'content' => "PeopleGoal is a cloud based employee performance management software platform for high-growth teams. Engage your employees with recognition, rewards, surveys and news. Manage your team members with core HR features including absence management, asset tracking and onboarding. Boost performance with SMART goals, OKRs, 360 degree feedback, one-to-ones and performance reviews.", 'link' => "https://www.peoplegoal.com/", 'views' => "19", 'status' => '1'],
                ['post_title'  => "Zugata", 'featured_image' => "https://i2.wp.com/workingtech.co/wp-content/uploads/2018/03/Zugata-Feature.png?fit=750%2C371", 'summary' => "Create a high-performance culture.", 'content' => "<h4>Why Zugata exists</h4>
                Zugata is the only performance management software built for both performance evaluation and performance development. 
                <br /><br />
                Performance management should help create a culture of high performance. Zugata is the only performance management software designed to help employees grow and improve, driving performance forward and impacting your company’s bottom line.", 'link' => "https://app.zugata.com/", 'views' => "14", 'status' => '1'],
                ['post_title'  => "Trakstar", 'featured_image' => "https://i.ytimg.com/vi/hVWL04AovfQ/maxresdefault.jpg", 'summary' => "Simple employee evaluation software anyone can use.", 'content' => "Trackstar built a product that helps companies align goals, give instant feedback to peers and make sure reviews are done well.
                Trackstar's been striving to revolutionize the world of employee evaluations by creating user-friendly yet powerful performance management tools. ", 'link' => "https://www.trakstar.com/", 'views' => "19", 'status' => '1'],
                ['post_title'  => "Bamboohr", 'featured_image' => "https://www.bamboohr.com/images/metadata/bamboohr-homepage.png", 'summary' => "HR software for small and medium businesses.", 'content' => "With BambooHR's optional implementation service, a dedicated HR Project Manager works with you one-on-one to transfer your organization’s data into your new BambooHR package.", 'link' => "https://www.bamboohr.com/", 'views' => "11", 'status' => '1'],
                ['post_title'  => "BetterWorks", 'featured_image' => "https://mlt71hewhsjv.i.optimole.com/w:auto/h:auto/q:auto/https://www.techfunnel.com/wp-content/uploads/2017/12/Here-Is-How-You-Should-Be-Using-BetterWorks.jpg", 'summary' => "BetterWorks is an enterprise goals platform for driving operational excellence and providing powerful insights about how work gets done.", 'content' => "<h4>Why BetterWorks exists</h4>
                Guide managers and employees to have regular, lightweight conversations around performance, feedback, development and recognition.", 'link' => "http://www.betterworks.com/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "15five", 'featured_image' => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4IR7kjBrqllPPMcHFbQihj5lJF8BkUle7GOibk15mNxQBWCQE&s", 'summary' => "Lightweight performance management via a full suite of integrated tools; employee feedback, OKR tracking, pulse surveys, 1-on-1 agendas, and peer recognition.", 'content' => "<h4>Why 15five exists</h4>
                15Five makes continuous employee feedback simple to drive high performing cultures.
                <br /><br />
                15Five uses proven employee development methods that drive businesses forward, unlocking the potential of every member of the global workforce.", 'link' => "https://www.15five.com/", 'views' => "12", 'status' => '1'],
                ['post_title'  => "Perdoo", 'featured_image' => "https://image.slidesharecdn.com/okrs-6mostimportantbenefitsofobjectiveskeyresults-150205185506-conversion-gate02/95/the-6-major-benefits-of-okr-8-638.jpg?cb=1423238046", 'summary' => "Perdoo is an OKR tool built to increase transparency, foster collaboration and enhance engagement within any organization.", 'content' => "Goal management software that helps deliver the results that matter and accelerate growth.
                <br /><br />
                Successfully bridge the gap between strategy & execution through clear, measurable OKRs. Perdoo helps you thrive in today's dynamic environment.
                <br /><br />
                All Perdoo customers benefit from coaching, designed to help you get the most from your investment in OKR.", 'link' => "https://www.perdoo.com/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "Slack", 'featured_image' => "https://i.pcmag.com/imagery/reviews/07td46ju7p6lLVb0QGwc5VF-6.fit_scale.size_2698x1517.jpg", 'summary' => "Slack is a team communication application providing real-time messaging, archiving and search for modern teams.", 'content' => "The collaboration software that moves work forward.
                <br /><br />
                Teamwork in Slack happens in channels — a single place for messaging, tools and files — helping everyone save time and collaborate together.
                <br /><br />
                Slack brings all your communication together.", 'link' => "https://slack.com/", 'views' => "2", 'status' => '1'],
                ['post_title'  => "Twist", 'featured_image' => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReOyKiXMj4u9KaBTZXoXqASsXIrpZvlia_lTG_DkqPaEPORy41&s", 'summary' => "Twist keeps your conversations on-topic and in one place. For teams who want to make work calmer, more organized, and more productive.", 'content' => "<h4>Why Twist exists</h4>
                Twist was built for a more intentional way of working together
                <br /><br />
                Twist gives your team an organized hub to discuss ideas, share updates, and build knowledge that everyone can refer back to – even years later.
                <br /><br />
                With clearly titled threads and powerful search, your team will be able to find any topic or decision. It’s browseable, searchable team knowledge that builds itself.", 'link' => "https://twistapp.com/", 'views' => "32", 'status' => '1'],
                ['post_title'  => "Igloo", 'featured_image' => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREP4RjXFR9LExJQIv_YXM17QEmIDGqR75nMRkYw-4_GrJYwx08&s", 'summary' => "Igloo digital workplace solutions connect people, processes, and information to create a more productive and engaged workforce.", 'content' => "Igloo enables organizations to move beyond a traditional intranet to a digital workplace; a destination that brings people and resources together to solve critical business challenges — and cultivate a strong corporate culture.
                <br /><br />
                Igloo integrates with the full Office 365 suite out of the box, making it easy to take advantage of the benefits these applications offer, and the investment made by your organization. Employees can easily access O365 content and apps all within their Igloo digital workplace.", 'link' => "http://www.igloosoftware.com/", 'views' => "3", 'status' => '1'],
                ['post_title'  => "Honey", 'featured_image' => "https://miro.medium.com/max/2568/1*P-mzoa7W9GeliLl0WeVrXQ.png", 'summary' => "Honey is your company’s go-to place for announcements, industry news, HR policies, important files, and quick links to other tools.", 'content' => "Honey gives your employees a simple, central location to find all of the company information and resources they need.
                <br /><br />
                Whether setting up your intranet or simply looking for your employee handbook, you’ll get the hang of Honey in no time. Simple design, personalized feeds, and customized notifications make Honey as intuitive as the apps you use and love outside of the office.
                <br /><br />
                Honey makes it easy to organize, share, and access beautiful embedded content and rich media. Files, videos, images, calendars, surveys? You got it.", 'link' => "https://honey.is/", 'views' => "16", 'status' => '1'],
                ['post_title'  => "Workplace by Facebook", 'featured_image' => "https://79chrrj514-flywheel.netdna-ssl.com/wp-content/uploads/2017/09/4d168d1e-d9b4-48ee-8b37-85847ea08fc0-2-1920x960.png", 'summary' => "Connect your whole organization with familiar tools, helping everyone in your business turn ideas into action.", 'content' => "Empower and transform your whole business, with familiar features like groups, chat and video calls.
                <br /><br />
                When someone posts in a different language, Workplace offers to translate it there and then. Helping you become a truly global business.
                <br /><br />
                Groups are spaces for sharing updates, files, feedback and more. They're like email threads, but better organized and easier to follow.", 'link' => "https://www.facebook.com/workplace", 'views' => "7", 'status' => '1'],
                ['post_title'  => "HeyTaco!", 'featured_image' => "https://www.heytaco.chat/public/images/press/ht-logo-full-web.png", 'summary' => "Reward your teammates with tacos in Slack when they say or do awesome", 'content' => "HeyTaco! sparks conversations and builds stronger relationships with its fun and unique kindness currency.
                <br /><br />
                Bring company values to life
                Make giving tacos matter more by tagging them with company values, initiatives, or anything else you can think of.", 'link' => "https://www.heytaco.chat/", 'views' => "32", 'status' => '1'],
                ['post_title'  => "Growbot", 'featured_image' => "https://miro.medium.com/max/638/1*5uD0yaxaNnr9MASLTB-F9Q.jpeg", 'summary' => "The bot that helps you recognize and appreciate your team", 'content' => "Growbot makes it easy and fun to celebrate great work when and where it happens.
                <br /><br />
                Growbot celebrates great work where it's happening and saves it for later. View your score, the leaderboard, and more.
                Having Growbot in your slack channel, for example, serves as an easy reminder to give props and appreciate others you work with regularly.", 'link' => "https://growbot.io/", 'views' => "2", 'status' => '1'],
                ['post_title'  => "YouEarnedIt", 'featured_image' => "https://findvectorlogo.com/wp-content/uploads/2018/12/youearnedit-vector-logo.png", 'summary' => "YouEarnedIt is an employee engagement software to recognize and reward people for behaviors aligned with core values and business objectives.", 'content' => "YouEarnedIt helps empower and align everyone around the company's core values, deliver fun and personalizes rewards, and equip them with powerful cultural insights.
                <br /><br />
                When you make employee engagement simple, you make it powerful.
                <br /><br />
                Real-time, meaningful recognition. Personalized, powerful rewards. Insights and analytics. ", 'link' => "https://youearnedit.com/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "Bonusly", 'featured_image' => "https://www.worksighted.com/wp-content/uploads/2017/02/Bounsly.png", 'summary' => "Bonusly helps companies reward and motivate employees using peer bonuses.", 'content' => "Bonusly is an engaging recognition and rewards platform that enriches your company culture.
                <br /><br />
                Bonusly integrates seamlessly with your organization's HR and communication tools, send microbonuses directly through Slack, view bonus activity on Office 365, automatically sync users in Workday.", 'link' => "https://bonus.ly/", 'views' => "2", 'status' => '1'],
                ['post_title'  => "Propsboard", 'featured_image' => "http://www.propsboard.com/wp-content/uploads/2016/05/PropsTVGraphSmall.png", 'summary' => "Upvote recognition and more from Slack to your office TVs.", 'content' => "Propsboard gives you full control of your TVs. Add any app from our app directory and chose when it should be displayed.
                Give shout outs, post images, and project general tom foolery from Slack to your office TVs.
                <br /><br />
                TURN YOUR TVS INTO LIVE, DYNAMIC BILLBOARDS
                <br /><br />
                By connecting services like Slack, you can crowdsource what gets displayed. Just use our special
                “props” reaction to upvote messages. This then streams to your office TVs.", 'link' => "http://www.propsboard.com/", 'views' => "13", 'status' => '1'],
                ['post_title'  => "Fond", 'featured_image' => "https://media-exp1.licdn.com/dms/image/C561BAQGn7XGlYlcwFw/company-background_10000/0?e=2159024400&v=beta&t=suKdJQ0kCCCDgqleyG6Ztt8T4KYXCa7ANRWOf8RXWGY", 'summary' => "Fond provides and manages corporate discounts and a rewards solution for companies.", 'content' => "Fond's easy-to-use and seamless platform enables all employees to quickly redeem corporate perks and rewards and send recognitions.
                <br /><br />
                Customize your Fond experience by creating unique recognition occasions and meaningful rewards tailored to your company.
                <br /><br />
                Get the data you need and track your program performance, usage, redemption history, popular perks and rewards, and more.", 'link' => "https://fond.co/", 'views' => "19", 'status' => '1'],
                ['post_title'  => "Bob", 'featured_image' => "https://media-exp1.licdn.com/dms/image/C4E1BAQElrvhD61qBUQ/company-background_10000/0?e=2159024400&v=beta&t=7uzJEr4k-PktXAPxX9UU6nOqfWvlnDgAaAy2CQa5_I4", 'summary' => "Lightweight performance management via a full suite of integrated tools; employee feedback, OKR tracking, pulse surveys, 1-on-1 agendas, and peer recognition.", 'content' => "Bob is a people management platform that helps fast-growing companies attract, excite, and retain their people through data-driven HR tools.
                <br /><br />
                Hibob was built with the understanding that the world of work is changing. Hibob believes that people are more than just numbers, resources, or talent and that decisions made about them should be personalized, holistic, and data-driven.", 'link' => "https://www.hibob.com/", 'views' => "9", 'status' => '1'],
                ['post_title'  => "Captain401", 'featured_image' => "https://ph-files.imgix.net/1a8acd2a-04c6-499b-8bc5-0422b6a575e0?auto=format&fit=crop&h=512&w=1024", 'summary' => "Captain401 offers the easiest and most affordable 401(k) retirement plan for the modern workforce.", 'content' => "Captain401 makes it easy to offer a 401(k). We automate all of the administration and help your employees plan for their futures with built-in investment advising.
                <br /><br /> 
                Captain401 is here for your company now and in the future. Human Interest takes the hassle out of managing your company’s 401(k): compliance testing, recordkeeping, paperwork—even automating syncs with your payroll.", 'link' => "https://captain401.com/", 'views' => "19", 'status' => '1'],
                ['post_title'  => "Rise", 'featured_image' => "https://www.onlinemarketplaces.com/ext/resources/-1GOMS/Jobs/Misc/rise.png?1571781981", 'summary' => "Rise unifies HR + Benefits + Payroll into a simplified, personalized, all-in-one People Platform.", 'content' => "Rise brings all your people management requirements together into a fully-integrated, transparent, and easy-to-use solution. No more headaches over tax remittances, compliance, direct deposits, T4s, pay stubs, vacation tracking, or benefits administration. Our powerful and user-friendly HR platform saves you hours of inefficient paperwork, creating more time for taking care of your people—from recruitment through retirement.", 'link' => "https://risepeople.com/", 'views' => "23", 'status' => '1'],
                ['post_title'  => "GoCo", 'featured_image' => "https://hoodwork-production.s3.amazonaws.com/uploads/story/image/317007/GoCo.jpg", 'summary' => "GoCo is an HR software works with your existing payroll provider and insurance carrier so you can manage all your HR from one place.", 'content' => "GoCo is modern HR, benefits and payroll that automates your workflow, without forcing you to change your processes, policies, and providers.
                <br /><br /> 
                Our mission at GoCo is to free our customers from outdated, painful and complex HR and benefits administration. We hope by doing so, we can empower companies to focus on their own employees and mission.", 'link' => "https://www.goco.io/", 'views' => "19", 'status' => '1'],
                ['post_title'  => "Charlie", 'featured_image' => "https://i.ytimg.com/vi/lkwbM2DqmkI/maxresdefault.jpg", 'summary' => "The Free HR Software for Teams With Big Ideas", 'content' => "CharlieHR gives you more time to do more of what matters, and more of what you love.
                <br /><br />
                Instantly access essential information - from key documents to contact details - with adaptable permissions to keep things secure. Individual logins mean your team can take responsibility for updating their own profiles.
                <br /><br />
                Team Members can request different leave types, with a straightforward approval process for Team Leaders. See an overview of current and upcoming time off for your entire company. Personalise your working week and set restricted days in advance.", 'link' => "https://www.charliehr.com/", 'views' => "29", 'status' => '1'],
                ['post_title'  => "Hivy", 'featured_image' => "https://lh3.googleusercontent.com/MakoC5moCdlWBSw74Ks4ZLboRdiDLrCVnNLcfMyHN9Dx4qOkZ3IEdqNoKsguKjE44wcswu0tmqPgOj5pvgJ0OM33SZfVG3sDJC_ZJOgkXfgo_w", 'summary' => "Hivy is a cloud-based software and app that enable your team to request anything they need for the office.", 'content' => "Hivy makes it easy to collect employee requests, manage projects, and create a happier office.
                <br /><br />
                Give your employees an easy way to get in touch with their office operations teams for anything they need.
                <br /><br />
                Hivy helps you manage your team requests, so you can focus on managing your office and stay in control.", 'link' => "https://hivyapp.com/", 'views' => "13", 'status' => '1'],
                ['post_title'  => "Eatclub", 'featured_image' => "https://pbs.twimg.com/profile_images/1145755887690080262/VUyrBNT1.png", 'summary' => "Get lunch at work. Without the work. You click. We deliver. Everyone eats.", 'content' => "EAT Club is a virtual cafeteria that can satisfy everyone’s tastes by delivering personalized, delicious meals to office teams.
                <br /><br />
                Employees choose their favorite lunch from a curated menu of delicious options everyday, developed by award-winning chefs.
                <br /><br />
                Allow each team member to personalize his/her own snack box with our snack box program or we'll help you setup your own snack pantry (and even monitor consumption). You'll also have complete control from your administrator dashboard.", 'link' => "https://www.eatclub.com/", 'views' => "17", 'status' => '1'],
                ['post_title'  => "Poppin", 'featured_image' => "https://assets.fontsinuse.com/static/use-media-items/89/88398/full-2980x1980/5cd9b99e/poppin-logo-omnes.png?resolution=0", 'summary' => "Poppin makes beautiful office appliencies.", 'content' => "Poppin is designed to be the effortless one-stop solution for your entire workspace. Our breadth of colors lets you express yourself in your favorite shade or brand your office in your company color. Best of all? Our dedicated team of Workstylists is here to help you every step of the way.
                <br /><br />
                Poppin takes the guesswork and frustration out of furnishing your workspace. From personalized space planning services to desk accessories customized with your logo, our expert Workstylists are here to help you design an office that inspires great work.", 'link' => "http://www.poppin.com/", 'views' => "18", 'status' => '1'],
                ['post_title'  => "Lessonly", 'featured_image' => "https://fitsmallbusiness.com/wp-content/uploads/2017/11/lessonly-logo.png", 'summary' => "Lessonly is modern learning software used by teams to translate important work knowledge into Lessons that accelerate productivity.", 'content' => "Follow Lessonly’s six steps to better work—and find your organization’s Better Work Score.
                <br /><br />
                With Lessonly, companies and managers quickly transform knowledge into shareable lessons and resources, engage employees through interactive feedback loops, accelerate rep and team performance, and measure the impact of better learning across their organizations.", 'link' => "https://www.lessonly.com/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "WorkRamp", 'featured_image' => "https://venturebeat.com/wp-content/uploads/2019/06/Social.png?fit=400%2C209&strip=all", 'summary' => "Training software for the modern workforce.", 'content' => "Four solutions tailored specifically for your business. From driving revenue to increasing customer satisfaction, WorkRamp provides enablement software to help you build world-class teams.", 'link' => "https://www.workramp.com/", 'views' => "14", 'status' => '1'],
                ['post_title'  => "Udemy", 'featured_image' => "https://miro.medium.com/max/1200/1*HuQyl7_WMMzOfs8RIlQ-XA.png", 'summary' => "The fastest-growing online platform for on-demand learning.", 'content' => "Our mission at Udemy for Business is to help employees around the world do whatever comes next. Through our powerful content marketplace, employees always get the most up-to-date, relevant, and engaging content taught by the world’s leading experts. From development and IT to leadership, marketing, data science, design and much more, Udemy is where employees and companies choose to learn. Organizations can also host and distribute their own proprietary content within their unique Udemy for Business account.", 'link' => "https://business.udemy.com/", 'views' => "21", 'status' => '1'],
                ['post_title'  => "Obie", 'featured_image' => "https://image.slidesharecdn.com/6obieai-170511045832/95/obieai-500-demo-day-batch-20-1-638.jpg?cb=1494973386", 'summary' => "Obie is a virtual assistant to help you make faster decisions, be more informed and get shit done without leaving Slack.", 'content' => "Obie is a virtual assistant to help you make faster decisions, be more informed and get everything done without leaving slack.
                <br /><br />
                Simply ask Obie a question, and he'll instantly provide you with the answer. With Obie, all of your team members have access to an expert. Save time by avoiding repetitive questions, shoulder taps, and calls that halt your focus.", 'link' => "http://obie.ai/", 'views' => "1", 'status' => '1'],
                ['post_title'  => "Grovo", 'featured_image' => "https://assets.pcmag.com/media/images/423549-grovo-lms-logo.jpg?width=810&height=456", 'summary' => "Grovo is reimagining learning for today’s teams.", 'content' => "Grovo’s platform and content work together to make training engaging, effective, and available right at the moment of need. Use Grovo as your primary learning system, or integrate Grovo with your existing LMS to amplify and accelerate your learning programs.
                <br /><br />
                Easily create engaging, effective microlearning programs that magically reach the right people, at the right time.", 'link' => "https://www.grovo.com/", 'views' => "22", 'status' => '1'],
                ['post_title'  => "Homebase", 'featured_image' => "https://images.squarespace-cdn.com/content/v1/52851956e4b0d970d0600f3c/1544634991782-6QFK9CQZUJFY8L5PF1AW/ke17ZwdGBToddI8pDm48kKMnqFbGzWydt4wPA7rLuI5Zw-zPPgdn4jUwVcJE1ZvWEtT5uBSRWt4vQZAgTJucoTqqXjS3CfNDSuuf31e0tVFClMivctvVD7PNn94tEfWEqaYpwobqgVKZTrYropuulhiw3vwRwS_SVezmcz061pg/logo_homebase.jpeg", 'summary' => "The free employee management solution for local businesses -- shift scheduling, time tracking, and more!", 'content' => "Homebase makes managing hourly work easier for over 100,000 local businesses. With free employee scheduling, time tracking, team communication, and hiring, managers and employees can spend less time on paperwork and more time on growing their business.", 'link' => "https://joinhomebase.com/", 'views' => "13", 'status' => '1'],
                ['post_title'  => "Resource Guru", 'featured_image' => "https://image.slidesharecdn.com/resourcegurutechstartupjobsfair-140311110706-phpapp01/95/resource-guru-presentation-at-techstartupjobs-fair-london-2014-1-638.jpg?cb=1394536908", 'summary' => "The fast, simple way to schedule people, equipment and other resources online.", 'content' => "Resource Guru has the perfect balance of simplicity and detail - it's so easy to see at a glance what's going on in the team and where the resourcing issues might arise.
                <br /><br />
                Companies of all shapes and sizes are transforming the way they schedule their resources. By using Resource Guru, they're saving time and money and gaining a competitive edge.", 'link' => "https://resourceguruapp.com/", 'views' => "31", 'status' => '1'],
                ['post_title'  => "Humanity", 'featured_image' => "https://wcdn.humanity.com/wp-content/uploads/2019/07/newsroom-750x360.jpg", 'summary' => "Easy-To-Use Employee Scheduling Software.", 'content' => "Humanity is the leading cloud-based employee scheduling platform that accelerates schedule creation by up to 80 percent. Organizations of all sizes receive unprecedented insight into their operations, and actionable data to optimize staffing based on employee skill set, time-off/leave management, and staff availability.
                <br /><br />
                Humanity is offered as SaaS and can be deployed as a stand-alone solution in the cloud, or integrated quickly and easily with today’s leading HCM platforms and payroll solutions. ", 'link' => "https://www.humanity.com/", 'views' => "11", 'status' => '1'],
            ]);
           
    }
}
