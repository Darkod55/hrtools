$(document).ready(function () {

    //Category Filter
    let isTrue = true;
    $('.cat-item').click(function () {
        $(this).toggleClass('categoryBackground');
        let butonValue = $(this).attr('data-slug');
        console.log(butonValue)
        if (isTrue) {
            $('.single-card').not("." + butonValue).hide();
            $('.single-card').filter("." + butonValue).toggleClass('selected');
            isTrue = false;
            } else {
            $('.single-card').filter("." + butonValue).toggleClass('selected').toggle();
            if($('.selected').length == 0){
                $('.single-card').show()
                isTrue = true;
            }
        }
    })

    //Share on featured
    $("body").on("click", "#forshare", function () {
        $(this).toggleClass('sharebg');
        if($("#testfield").hasClass('testtt')) {
            $("#testfield").removeClass("testtt")
        } else {
            $("#testfield").addClass("testtt");
        }
    });

    //Share in modal
    $('body').on("click", "i.sharemodal", function () {
        $(this).toggleClass('sharebg');
        if($("p#sharebuttons").hasClass('testtt')) {
            $("p#sharebuttons").removeClass("testtt")
        } else {
            $("p#sharebuttons").addClass("testtt");
        }
    });

    //Search Field Show
    var fors = true;
    $('span.glyphicon.glyphicon-search.search-button').on("click", function() {
        if(fors == true) {
            $('#search-input').animate({
                'width': '180px'
            }, 400).show();
            fors = false;
        } else if(fors == false) {
            $('#search-input').animate({
                'width': '0'
            }, 400).hide(100);
            fors = true;
        }   
    });

    //Show Add New Company
    $(".cat15").on("click", function () {
        $(".addthiscomp").slideToggle();
        if($('.cat15').hasClass('categoryBackground')) {
            $('#postadd').css('display', 'inline-block');
            $('#postadd1').css('display', 'inline-block');
        } else {
            $('#postadd').hide();
            $('#postadd1').hide();
        }
        
    });

    //Show Add Company Form
    $("body").on("click", ".addthiscomp", function () {
        $(".allposts").hide(500);
        // $("#loginform").hide(500);
        // $("#featured").hide(500);
        $(".addformrow").show(500);
    });

    //Close Add Company Form
    $(".formclose").on("click", function () {
        $(this).parents('.addformrow').hide(500);
        $(".allposts").show(500);
    });
    $("body").on("click", ".buttonAdd", function () {
        $(".allposts").hide(500);
        // $("#loginform").hide(500);
        // $("#featured").hide(500);
        $(".addtoolrow").show(500);
    });

    //Add CSRF Token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Add New Company Request
    $('.addcompform').on('submit', function(e) {
        e.preventDefault();
        let objectToSubmit = {
            name: $('#name').val(),
            website: $('#website').val(),
            about: $('#about').val(),
            employee: $('input[type=radio][name=employee]:checked').val(),
            email: $('#emailAddCompany').val(),
            is_approved: 0
        }
        console.log(objectToSubmit);
        $.post("store", objectToSubmit).then(data => {
            $(".errorsmsg").hide();
            $(".successmsg").show();
            $(".successmsg").append("<li>Thanks for creating the company. Your request would be reviewed soon!</li>");
            $('#name').val('');
            $('#website').val('');
            $('#about').val('');
            $('#input[type=radio][name=employee]').val('');
            $('#emailAddCompany').val('');
        // let test = JSON.parse(data)
        console.log(data)
        }).catch(function(xhr, status, error) {
            // var errors = err.responseJSON
            $(".errorsmsg").show();
            $.each(xhr.responseJSON.errors, function (key, item) {
                $(".errorsmsg").append("<li>"+item+"</li>")
            });
            console.log(errors);
        })
    })

    //Function for appending posts
    function testFunction(value) {
        var categories = value.categories;
        var categoryarray = "";
        var categoryclass = "";
        $.each(categories, function(i, name) {
            categoryarray += name.title + " | ";
        })
        $.each(categories, function(i, name) {
            categoryclass += name.slug + " ";
        })
        categoryarray = categoryarray.substring(0,categoryarray.length - 2);
        
        $('.postscol').append(
            `
            <div class="card single-card ${categoryclass}" data-toggle="modal" data-target="#mod-card${value.id}" style="background-image: url('${value.featured_image}')">
                <div class="for-opacity">
                    <div class="card-content">
                        <p class="post-title">${value.post_title}</p>
                        <p class="post-cat">${categoryarray}</p>
                        <hr style="background-color: ${categories[0].color}" class="hr pull-left">   
                    </div>
                </div>
            </div>
            <div class="modal fade" id="mod-card${value.id}" tabindex="-1" role="dialog" aria-labelledby="${value.post_title}">
                <div class="modal-dialog model-cards" role="document">
                    <div class="modal-content">
                        <div class="modal-body modal-size">
                            <button type="button" class="close close-per" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="sidenav sidemodal">
                            <p class="post-title">${value.post_title}</p>
                            <div class="for-cat">
                                <p class="post-cat post-title-modal">${categoryarray}</p>
                                <hr style="background-color: ${categories[0].color}" class="hr hr-modal pull-left">
                            </div>
                        </div>
                        <div class="container-fluid inmodal">
                            <div class="row">
                                <div class="col-md-9 col-md-offset-3 formodal">
                                    <div class="forimage-test">
                                        <img class="cat-image img img-resposnive" src="${value.featured_image}" />
                                        <div class="testbg">
                                            <img class="imagefull" src="${value.featured_image}" />
                                        </div>
                                    </div>
                                    <div class="for-content-post">
                                        <p class="post-title">${value.post_title}</p>
                                        <div class="social">
                                            <i class="fal fa-share-alt sharemodal"></i>
                                            <p id="sharebuttons">                  
                                                <a href="http://www.facebook.com/sharer.php?u=meetcolony.com/hrtools" target="_blank"><i class="fab fa-facebook"></i></a>
                                                <a href="https://twitter.com/intent/tweet"><i class="fab fa-twitter-square"></i></a>
                                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=meetcolony.com/hrtools" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                            </p>     
                                        </div>
                                        <a href="${value.link}" target="_blank">Visit ${value.post_title}</a>
                                        <br />
                                        <h5>${value.summary}</h5>                                         
                                        <p class="content-modal">${value.content}</p>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            `) 
    }

    //Form for in modal subscription that I need to fix
    // <form class="navbar-form form-in-modal subscribeForm" action="{{ route('storeSubscribe') }}">
    //     <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> 
    //     <div class="form-group custom-form-group">
    //         <div class="form-group flex custom-form-group">
    //             <div class="input-group">
    //                 <span class="input-group-addon custom-form-icon"><i class="far fa-envelope"></i></span>
    //                 <input type="email" name="email" required class="form-control custom-form mailPosts" placeholder="Get two new tools every week" aria-describedby="basic-addon1">
    //             </div>
    //             <button class="btn submitBtn posts-btn" type="submit" value="Submit">Stay Updated</button>
    //         </div>
    //         <span id="posts-feedback"></span>
    //     </div>
    // </form>

    //For Featured Post
    // function forfeatured() {
    //     $.ajax({
    //         method: "GET",
    //         url: "showFeatured",
    //         success: function(data) {
    //             // console.log(data);
    //             $('.featuredcard').append(
    //             `
    //             <div class="bigcard card${data.id}">
    //                 <div class="col-md-6 col-xs-12 featuredimage" style="background-image: url('${data.featured_image}')">
    //                     <p></p>
    //                 </div>
    //                 <div class="col-md-5 colfeatured">
    //                     <div class="forfeatured">
    //                         <div class="card-content featcon">
    //                             <p class="post-title featured">${data.post_title}</p>
    //                             <p class="post-content featured">${data.summary}</p>
    //                             <button class="btn butaddcomp btnfeatured" style="background-color:${data.categories.color};" data-toggle="modal" data-target="#mod-card${data.id}">Find Out More</button>
    //                             <i class="fal fa-share-alt" id="forshare"></i><p id="testfield">
    //                             <a href="http://www.facebook.com/sharer.php?u=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-facebook"></i></a>
    //                             <a href="https://twitter.com/intent/tweet"><i class="fab fa-twitter-square"></i></a>
    //                             <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
    //                             </p>
    //                         </div>
    //                     </div>
    //                 </div>
    //             </div> 
    //             `)
    //     }          
    // })
    // }

    //For All Posts
    function forajax() {
        $.ajax({
            method: "GET",
            url: "showHome",
            success: function(data) {
                // console.log(data);
                $('.postscol').append(`
                    <div class="card addthiscomp" id="postadd">
                        <div class="card-contents">
                            <p class="post-plus">+</p>
                            <p class="post-add">ADD NEW COMPANY</p>
                        </div>
                    </div>
                    <div class="card" id="postadd1">
                        <a class="linklaika" href="https://www.wearelaika.com/" target="blank"><p class="forlaika"> </p></a>
                    </div>`)  
            data.forEach(value => {
                testFunction(value);
            })
        }     
    })
    }

    
   
    //Call these functions on page load
    // forfeatured();
    forajax();

    //For Search Functionality
    $('#search-input').on('keyup', function(e) {
        e.preventDefault();
        var searchvalue = $('#search-input').val();
        console.log(searchvalue);
        $.post('search', {
            'search': searchvalue
        }, function(data) {
            console.log(data);
            $('.postscol').empty();
            if(data.length == 0) {
                $('.postscol').append("<h2>We have no posts matching your search!</h2>"); 
            }   
            data.forEach(value => {
                testFunction(value);
            })
        })
    })

    //Subscribe form in header
    $('.subscribeForm').on('submit', function(e) {
        e.preventDefault();
        let objectToSubmit = {
            email: $('#subscribe').val(),
            status: 1
        }
        console.log(objectToSubmit);
        $.post("storeSubscribe", objectToSubmit).then(data => {
            $(".divsubscribe").html("<p class='text-right subscribemsg'>Thank you for subscribing. It is our promise that we will not spam you with useless emails</p>");
            // $(".successmsg").show();
            // $(".successmsg").append("<li>Thanks for creating the company. Your request would be reviewed soon!</li>")
        // let test = JSON.parse(data)
        console.log(data)
        }).catch(function(xhr, status, error) {
            $(".divsubscribe").html("<p class='text-right subscribemsg'>Please insert a valid email address</p>");
            // // var errors = err.responseJSON
            // $(".errorsmsg").show();
            // $.each(xhr.responseJSON.errors, function (key, item) {
            //     $(".errorsmsg").append("<li>"+item+"</li>")
            // });
            console.log(errors);
        })
    })

    //Dashboard show Companies
    function showCompanies() {
        $.ajax({
            method: "GET",
            url: "showCompanies",
            success: function(data) {
                console.log(data); 
                $('.wrapper').html(`
                        <h1>Approved Companies</h1>
                        <div class="appCompanies"></div>
                        <h1>Companies waiting for approval</h1>
                        <div class="waitCompanies"></div>
                    `)
                $('.appCompanies').append(`
                    <table class="table table-bordered table-hover tablelink">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Website</th>
                                <th>About</th>
                                <th>Email</th>
                                <th>Is Employed</th>
                                <th>Is Approved</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="appendcompanies">
                        
                        </tbody>
                    </table>
                `)
                $('.waitCompanies').append(`
                    <table class="table table-bordered table-hover tablelink">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Website</th>
                                <th>About</th>
                                <th>Email</th>
                                <th>Is Employed</th>
                                <th>Is Approved</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="waitcompanies">
                        
                        </tbody>
                    </table>
                `)
                data.forEach(value => {

                    if(value.employee == 1) {
                        var employee = "Yes";
                    } else {
                        var employee = "No";
                    }

                    if(value.is_approved == 1) {
                        var status = "Approved";
                        $('.appendcompanies').append(`
                            <tr>
                                <td>${value.id}</td>
                                <td>${value.name}</td>
                                <td>${value.website}</td>
                                <td>` + value.about.slice(0,20) + `...</td>
                                <td>${value.email}</td>
                                <td>${employee}</td>
                                <td>${status}</td>
                                <td>
                                    <button class="btn btn-warning"><a href="disapprove/${value.id}">Disapprove</a></button>
                                    <button class="btn btn-danger"><a href="delete/${value.id}">Delete</a></button>
                                </td>
                            </tr>
                        `)
                    } else if(value.is_approved != 1) {
                        var status = "Not Approved";
                        $('.waitcompanies').append(`
                            <tr>
                                <td>${value.id}</td>
                                <td>${value.name}</td>
                                <td>${value.website}</td>
                                <td>` + value.about.slice(0,20) + `...</td>
                                <td>${value.email}</td>
                                <td>${employee}</td>
                                <td>${status}</td>
                                <td>
                                    <button class="btn btn-info"><a href="approve/${value.id}">Approve</a></button>
                                    <button class="btn btn-danger"><a href="delete/${value.id}">Delete</a></button>
                                </td>
                            </tr>
                        `)
                    }
                    
                })
            }     
        })
    }
    showCompanies();

    //Dashboard show Subscribers
    $('.forsubscribers').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            method: "GET",
            url: "showSubscribers",
            success: function(data) {
                console.log(data); 
                $('.wrapper').html(`
                    <h1>Subscribed Users</h1>
                    <div class="subscribers"></div>
                `)
                $('.subscribers').append(`
                    <table class="table table-bordered table-hover tablelink">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="appendsubscribers">
                        
                        </tbody>
                    </table>
                `)
                data.forEach(value => {
                    if(value.status == 1) {
                        var subscribed = "Subscribed";
                    }
                    $('.appendsubscribers').append(`
                        <tr>
                            <td>${value.id}</td>
                            <td>${value.email}</td>
                            <td>${subscribed}</td>
                            <td>
                                <button class="btn btn-danger"><a href="deleteSubscriber/${value.id}">Delete</a></button>
                            </td>
                        </tr>
                    `)
                })
            }     
        })
    })

    //Dashboard show Companies on click
    $('.forcompanies').on('click', function(e) {
        showCompanies();
    })

    //Dashboard show Posts
    $('.forposts').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            method: "GET",
            url: "showPosts",
            success: function(data) {
                console.log(data); 
                $('.wrapper').html(`
                    <h1>Posts</h1>
                    <div class="pending-posts"></div>
                    <div class="allposts"></div>
                `)
                
                $('.allposts').append(`
                    <table class="table table-bordered table-hover tablelink">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Summary</th>
                                <th>Content</th>
                                <th>Link</th>
                                <th>Views</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="appendposts">
                        
                        </tbody>
                    </table>
                `)
                data.forEach(value => {
                    if(value.status == 0) {
                        $('.pending-posts').append(`
                            <table class="table table-bordered table-hover tablelink testtable">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Summary</th>
                                        <th>Content</th>
                                        <th>Link</th>
                                        <th>Views</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="appendpending">
                                
                                </tbody>
                            </table>
                        `)
                        $('.appendpending').append(`
                            <tr>
                                <td><img src="${value.featured_image}" /></td>
                                <td>${value.id}</td>
                                <td>${value.post_title}</td>
                                <td>` + value.summary.slice(0,20) + `</td>
                                <td>` + value.content.slice(0,20) + `</td>
                                <td>` + value.link.slice(0,20) + `</td>
                                <td>${value.views}</td>
                                <td>
                                    <button class="btn btn-warning"><a href="approvePost/${value.id}">Approve</a></button>
                                    <button class="btn btn-danger"><a href="deletePost/${value.id}">Delete</a></button>
                                </td>
                            </tr>
                        `)
                        }
                    else {
                        $('.appendposts').append(`
                            <tr>
                                <td><img src="${value.featured_image}" /></td>
                                <td>${value.id}</td>
                                <td>${value.post_title}</td>
                                <td>` + value.summary.slice(0,20) + `</td>
                                <td>` + value.content.slice(0,20) + `</td>
                                <td>` + value.link.slice(0,20) + `</td>
                                <td>${value.views}</td>
                                <td>
                                    <button class="btn btn-warning"><a href="editPost/${value.id}">Edit</a></button>
                                    <button class="btn btn-danger"><a href="deletePost/${value.id}">Delete</a></button>
                                </td>
                            </tr>
                        `)
                    }
                })
            }     
        })
    })

    $('.navbar1').on('click', function() {
        if($('#navbar2').hasClass('in')) {
            $('#navbar2').removeClass('in');
        }
    })

    $('.navbar2').on('click', function() {
        if($('#navbar1').hasClass('in')) {
            $('#navbar1').removeClass('in');
        }
    })

    //Add New Post Request
    $('.addtoolform').on('submit', function(e) {
        e.preventDefault();
        let testObject = {
            post_title: $('#post_title').val(),
            link: $('#link').val(),
            summary: $('#summary').val(),
            social: $('#social').val(),
            category_id: $('#category_id').val(),
            views: 0,
            status: 0,
            email: $('#emailContributers').val()
        }
        console.log(testObject);
        $.post("storePost", testObject).then(data => {
            $(".errorsmsg").hide();
            $(".successmsg").empty();
            $(".successmsg").show();
            $(".successmsg").append("<li>Thank you for your support, we will review the product and include it in the list!</li>");
            $('#post_title').val('');
            $('#link').val('');
            $('#summary').val('');
            $('#social').val('');
            $('#category_id').val('');
            $('#emailContributers').val('');
        // let test = JSON.parse(data)
        console.log(data)
        }).catch(function(xhr, status, error) {
            // var errors = err.responseJSON
            $(".successmsg").hide();
            $(".errorsmsg").empty();
            $(".errorsmsg").show();
            $(".errorsmsg").append("<li>Please fill in all the required fields. Thank you.</li>");
            $.each(xhr.responseJSON.errors, function (key, item) {
                $(".errorsmsg").append("<li>"+item+"</li>")
            });
            console.log(errors);
        })
    })
})