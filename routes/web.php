<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function () {
    return view('test');
});

Route::get('/', 'HomepageController@index')->name('Index');
Route::get('dashboard', 'DashboardController@index')->name('IndexDash');        
Route::post('/store', 'CompanyController@store')->name('addCompany');
Route::post('/storeSubscribe', 'SubscribeController@storeSubscribe')->name('storeSubscribe');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/showHome', 'HomepageController@showHome');
Route::get('/showFeatured', 'HomepageController@showFeatured');
Route::get('/showCompanies', 'DashboardController@showCompanies');
Route::get('/showSubscribers', 'DashboardController@showSubscribers');
Route::get('/showPosts', 'DashboardController@showPosts');
Route::get('/delete/{id}', 'DashboardController@destroy');
Route::get('/deleteSubscriber/{id}', 'DashboardController@destroySubscriber');
Route::get('/deletePost/{id}', 'DashboardController@destroyPost');
Route::get('/approve/{id}', 'DashboardController@approve');
Route::get('/disapprove/{id}', 'DashboardController@disapprove');
Route::get('/approvePost/{id}', 'DashboardController@approvePost');
Route::get('/editPost/{id}', 'DashboardController@editPost');
Route::post('/updatePost', 'DashboardController@updatePost')->name('UpdatePost');
// Route::post('/addTool', 'DashboardController@addTool')->name('addTool');
Route::post('search', 'HomepageController@search');
Route::post('/storePost', 'HomepageController@storePost')->name('storePost');
Route::post('/login', 'Auth\LoginController@login')->name('login');

